#!/bin/bash

#script for compiling and uploading to multiple ESP32 boards at once. Usage: ./upload.sh <number of boards>


upload (){
  make flash ESPPORT=/dev/ttyUSB$1
}

killall minicom
make &&
for ((i=0; i < $1; i+=1)); do
  upload $i &
done
wait
