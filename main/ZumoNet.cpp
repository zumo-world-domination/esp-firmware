#include "ZumoNet.h"
#include "Hardware.h"
#include "systemFlags.h"
#include <list>
#include <freertos/queue.h>
#include "esp_wifi.h"
#include "esp_system.h"
#include <cJSON.h>
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>
#include <esp_event_loop.h>

#define  MULTICAST_TTL 1

using namespace zumoNet;

static const char* TAG = "ZUMONET";
static const char heartbeatTopic[] = "heartbeat";

static std::list<struct messageSubscription> subscriptions;
SemaphoreHandle_t subscriptionMutex;

struct heartbeatTimestamp{
  TickType_t time;
  char address[sizeof(nodeId)];
  uint8_t n;
};
std::list<struct heartbeatTimestamp> heartbeatTimestamps;

static int sock;

RingbufHandle_t rcvBuf;
//static uint8_t broadcast_mac[ESP_NOW_ETH_ALEN] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
//static uint16_t espnow_seq[ESPNOW_DATA_MAX] = { 0, 0 };

char zumoNet::nodeId[16];
RingbufHandle_t zumoNet::subscribe(std::string topic){
  xSemaphoreTake(subscriptionMutex, portMAX_DELAY);
  RingbufHandle_t buf_handle;
  buf_handle = xRingbufferCreate(SUBSCRIBER_BUFFER_SIZE, RINGBUF_TYPE_NOSPLIT);
  struct messageSubscription newSubscription = {topic, buf_handle};
  subscriptions.push_back(newSubscription);
  xSemaphoreGive(subscriptionMutex);
  return buf_handle;
}

void zumoNet::unsubscribe(RingbufHandle_t buffer){
  xSemaphoreTake(subscriptionMutex, portMAX_DELAY);
  for (std::list<struct messageSubscription>::iterator it=subscriptions.begin(), end = subscriptions.end(); it!=end;){
    RingbufHandle_t thisBuf = it->buffer;
    if(thisBuf == buffer){
      it = subscriptions.erase(it);
    }else{
      ++it;
    }
  }
  vRingbufferDelete(buffer);
  xSemaphoreGive(subscriptionMutex);
}

static int socket_add_ipv4_multicast_group(int sock, bool assign_source_if)
{
  struct ip_mreq imreq = { 0 };
  struct in_addr iaddr = { 0 };
  int err = 0;
  // Configure source interface
  imreq.imr_interface.s_addr = IPADDR_ANY;
  // Configure multicast address to listen to
  err = inet_aton(CONFIG_MULTICAST_IPV4_ADDR, &imreq.imr_multiaddr.s_addr);
  if (err != 1) {
    ESP_LOGE(TAG, "Configured IPV4 multicast address '%s' is invalid.", CONFIG_MULTICAST_IPV4_ADDR);
    return  err;
  }
  ESP_LOGI(TAG, "Configured IPV4 Multicast address %s", inet_ntoa(imreq.imr_multiaddr.s_addr));
  if (!IP_MULTICAST(ntohl(imreq.imr_multiaddr.s_addr))) {
    ESP_LOGW(TAG, "Configured IPV4 multicast address '%s' is not a valid multicast address. This will probably not work.", CONFIG_MULTICAST_IPV4_ADDR);
  }

  if (assign_source_if) {
    // Assign the IPv4 multicast source interface, via its IP
    // (only necessary if this socket is IPV4 only)
    err = setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, &iaddr,
                     sizeof(struct in_addr));
    if (err < 0) {
      ESP_LOGE(TAG, "Failed to set IP_MULTICAST_IF. Error %d", errno);
      return  err;
    }
  }

  err = setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                   &imreq, sizeof(struct ip_mreq));
  if (err < 0) {
    ESP_LOGE(TAG, "Failed to set IP_ADD_MEMBERSHIP. Error %d", errno);
    return  err;
  }
  return err;
}
static int create_multicast_ipv4_socket()
{
    struct sockaddr_in saddr = { 0 };
    int sock = -1;
    int err = 0;

    sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock < 0) {
        ESP_LOGE(TAG, "Failed to create socket. Error %d", errno);
        return -1;
    }

    // Bind the socket to any address
    saddr.sin_family = PF_INET;
    saddr.sin_port = htons(CONFIG_UDP_PORT);
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    err = bind(sock, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in));
    if (err < 0) {
        ESP_LOGE(TAG, "Failed to bind socket. Error %d", errno);
        close(sock);
    return -1;
    }


    // Assign multicast TTL (set separately from normal interface TTL)
    uint8_t ttl = MULTICAST_TTL;
    setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(uint8_t));
    if (err < 0) {
        ESP_LOGE(TAG, "Failed to set IP_MULTICAST_TTL. Error %d", errno);
        close(sock);
    return -1;
    }

    // select whether multicast traffic should be received by this device, too
    // (if setsockopt() is not called, the default is no)
    uint8_t loopback_val = 0;
    err = setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP,//TODO check this option to stop double receive
                     &loopback_val, sizeof(uint8_t));
    if (err < 0) {
        ESP_LOGE(TAG, "Failed to set IP_MULTICAST_LOOP. Error %d", errno);
        close(sock);
    return -1;
    }

    // this is also a listening socket, so add it to the multicast
    // group for listening...
    err = socket_add_ipv4_multicast_group(sock, true);
    if (err < 0) {
        close(sock);
    return -1;
    }

    // All set, socket is configured for sending and receiving
    return sock;
}



static void handleHeartbeat(const char* node, uint8_t n){
  if(n != getNumNodes()){
    xEventGroupClearBits(systemFlags, N_KNOWN);
    ESP_LOGW(TAG, "Number of nodes inconsistent");
  }
  bool knownNode = false;
  for(auto &it: heartbeatTimestamps){
    if(!strncmp(it.address, node, strlen(nodeId))){
      it.time = xTaskGetTickCount();
      it.n = n;
      knownNode = true;
    }
  }
  if(!knownNode){
    ESP_LOGI(TAG, "New node %s", node);//TODO reduce log level
    struct heartbeatTimestamp timestamp = {};
    timestamp.time = xTaskGetTickCount();
    timestamp.n = n;
    strncpy(timestamp.address, node, sizeof(nodeId));
    heartbeatTimestamps.push_back(timestamp);
  }
  if(!(xEventGroupGetBits(systemFlags) & N_KNOWN)){//Check if we have consensus on N again
    bool N_consistent = true;
    for(auto &it: heartbeatTimestamps){
      if(it.n != getNumNodes()){
        N_consistent = false;
      }
    }
    if(N_consistent){
      ESP_LOGI(TAG, "Consensus on N regained");
      xEventGroupSetBits(systemFlags, N_KNOWN);
    }
  }
}

static void handleRx(const char *data){
  cJSON *root = cJSON_Parse(data);
  {
  if(root == NULL){
    ESP_LOGE(TAG, "Failed to parse JSON message %s",data);
    return;
  }
  cJSON *sender = cJSON_GetObjectItem(root, "sender");
  if(sender == NULL || sender->valuestring == NULL){
    ESP_LOGW(TAG, "Missing sender: %s",data);
    goto end;
  }
  cJSON *topic = cJSON_GetObjectItem(root, "topic");
  if(sender == NULL || sender->valuestring == NULL){
    ESP_LOGW(TAG, "Missing topic: %s",data);
    goto end;
  }
  cJSON *msg = cJSON_GetObjectItem(root, "msg");

  if(msg == NULL || msg->valuestring == NULL){
    ESP_LOGW(TAG, "Missing message: %s",data);
    goto end;
  }

  if(!strncmp(heartbeatTopic, topic->valuestring, sizeof(heartbeatTopic) -1)){
    ESP_LOGD(TAG, "heartbeat");
    handleHeartbeat(sender->valuestring, atoi(msg->valuestring));
  }else{

    size_t size = strlen(msg->valuestring)+1+sizeof(nodeId);
    struct message *payload = (struct message*) malloc(size);
    strncpy(payload->sender,sender->valuestring, sizeof(nodeId));
    strcpy(payload->message, msg->valuestring);
    for(auto const &it: subscriptions){
      if(!strcmp(topic->valuestring, it.topic.c_str())){
        UBaseType_t res =  xRingbufferSend(it.buffer, payload, size, pdMS_TO_TICKS(100));
        if(res != pdTRUE){
          ESP_LOGE(TAG, "dropped message for subscription %s", it.topic.c_str());
        }

      }
    }
    free(payload);
  }
}
 end:
  cJSON_Delete(root);
}
  static void heartbeat_task(void *pvParams){
    char numString[] = "00";
    for(;;){
      xEventGroupWaitBits(systemFlags, MULTICAST_SOCKET_OPEN, pdFALSE, pdTRUE, portMAX_DELAY);
      ESP_LOGD(TAG, "Sending heartbeat");
      sprintf(numString, "%x", getNumNodes());
      broadcast("heartbeat", numString);
      vTaskDelay(pdMS_TO_TICKS(CONFIG_HEARTBEAT));
      TickType_t time = xTaskGetTickCount();
      for(std::list<struct heartbeatTimestamp>::iterator it=heartbeatTimestamps.begin(); it!=heartbeatTimestamps.end(); ++it){
        if(time - it->time > pdMS_TO_TICKS(CONFIG_HEARTBEAT * 5)){
          ESP_LOGE(TAG, "Node %s timed out", it->address);
          heartbeatTimestamps.erase(it);
          break;
        }
        }
    }
  }

  void openSocket(){
    sock = create_multicast_ipv4_socket();
    if (sock < 0) {
      xEventGroupClearBits(systemFlags, MULTICAST_SOCKET_OPEN);
      ESP_LOGE(TAG, "Failed to create IPv4 multicast socket");
    }


    // set destination multicast addresses for sending from these sockets
    struct sockaddr_in sdestv4 = {
    };
    sdestv4.sin_family = PF_INET;
    sdestv4.sin_port = htons(CONFIG_UDP_PORT);

    // We know this inet_aton will pass because we did it above already
    inet_aton(CONFIG_MULTICAST_IPV4_ADDR, &sdestv4.sin_addr.s_addr);
    xEventGroupSetBits(systemFlags, MULTICAST_SOCKET_OPEN);
  }



  static esp_err_t wifi_event_handler(void *ctx, system_event_t *event)
  {
    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
      ESP_LOGI(TAG, "Wifi STA start");
      esp_wifi_connect();
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      ESP_LOGI(TAG, "Wifi STA connected");
      xEventGroupSetBits(systemFlags, WIFI_CONNECTED);
      strcpy(nodeId,ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
      openSocket();
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      ESP_LOGI(TAG, "Wifi STA disconnected");
      esp_wifi_connect();
      xEventGroupClearBits(systemFlags, WIFI_CONNECTED);
      break;
    default:
      break;
    }
    return ESP_OK;
  }


  static void wifi_init(void)
  {
    uint8_t mac[6];
    esp_efuse_read_mac(mac);
    //sprintf(nodeId, "%02x%02x%02x%02x%02x%02x", mac[0], mac[1], mac[2], mac[3], mac[4],mac[5]);
    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(wifi_event_handler, NULL));
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
    wifi_config_t wifi_config = {};

    strcpy((char*)wifi_config.sta.ssid, CONFIG_WIFI_SSID);
    strcpy((char*)wifi_config.sta.password, CONFIG_WIFI_PASSWORD);
    wifi_config.sta.channel = CONFIG_WIFI_CHANNEL;

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config));
    ESP_LOGI(TAG, "Connecting to WIFI SSID: %s", (char*)wifi_config.sta.ssid);
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGI(TAG, "Waiting for wifi");
    //  xEventGroupWaitBits(systemFlags, WIFI_CONNECTED, false, true, portMAX_DELAY);
  }

static void rxHandlerTask(void *nothing){
  for(;;){
    size_t size;
    void *item = xRingbufferReceive(rcvBuf, &size, portMAX_DELAY);
    handleRx((char*)item);
    vRingbufferReturnItem(rcvBuf,item);
  }
}

  static char recvbuf[1500];
  static void multicastRxTask(void*args){
    while (1) {
      /* Wait for all the IPs we care about to be set
       */
      ESP_LOGI(TAG, "Waiting for multicast socket");
      xEventGroupWaitBits(systemFlags, MULTICAST_SOCKET_OPEN, false, true, portMAX_DELAY);
      ESP_LOGI(TAG, "Socket opened");

      // Loop waiting for UDP received
      int err = 1;
      while (err > 0) {
        //vTaskDelay(pdMS_TO_TICKS(500));
        struct timeval tv = {

        };
        tv.tv_sec = 2;
        tv.tv_usec = 0;
        fd_set rfds;
        FD_ZERO(&rfds);
        FD_SET(sock, &rfds);

        int s = select(sock + 1, &rfds, NULL, NULL, &tv);
        if (s < 0) {
          ESP_LOGE(TAG, "Select failed: errno %d", errno);
          err = -1;
          break;
        }
        else if (s > 0) {
          if (FD_ISSET(sock, &rfds)) {
            // Incoming datagram received
            char raddr_name[32] = { 0 };

            struct sockaddr_in6 raddr; // Large enough for both IPv4 or IPv6
            socklen_t socklen = sizeof(raddr);
            int len = recvfrom(sock, recvbuf, sizeof(recvbuf)-1, 0,
                               (struct sockaddr *)&raddr, &socklen);
            if (len < 0) {
              ESP_LOGE(TAG, "multicast recvfrom failed: errno %d", errno);
              err = -1;
              break;
            }

            // Get the sender's address as a string
            if (raddr.sin6_family == PF_INET) {
              inet_ntoa_r(((struct sockaddr_in *)&raddr)->sin_addr.s_addr,
                          raddr_name, sizeof(raddr_name)-1);
            }
            ESP_LOGI(TAG, "received %d bytes from %s:", len, raddr_name);
            recvbuf[len] = 0; // Null-terminate whatever we received and treat like a string...
            ESP_LOGI(TAG, "%s", recvbuf);
            if(xRingbufferSend(rcvBuf, recvbuf, len, 0) != pdTRUE){
              ESP_LOGE(TAG, "UDP receive buffer full!");
            }
            //handleRx(recvbuf);
          }

        }
        else {//socket timed out
        }
      }
      ESP_LOGE(TAG, "Shutting down socket and restarting...");
      shutdown(sock, 0);
      close(sock);
      xEventGroupClearBits(systemFlags, MULTICAST_SOCKET_OPEN);
    }
  }

  void zumoNet::init(int core){
    rcvBuf = xRingbufferCreate(5000, RINGBUF_TYPE_NOSPLIT);
    wifi_init();
    subscriptionMutex = xSemaphoreCreateMutex();
    xTaskCreatePinnedToCore(
                            multicastRxTask, // Task function.
                            "", //name of task.
                            4096, //Stack size of task
                            NULL, // parameter of the task
                            15,  // priority of the task
                            NULL, //Task handle to keep track of created task
                            core); //core to run task on. Core 0 = WiFi, Core 1 = Arduino
    xTaskCreatePinnedToCore(
                            heartbeat_task, // Task function.
                            "", //name of task.
                            4096, //Stack size of task
                            NULL, // parameter of the task
                            10,  // priority of the task
                            NULL, //Task handle to keep track of created task
                            1); //core to run task on. Core 0 = WiFi, Core 1 = Arduino
    xTaskCreatePinnedToCore(
                            rxHandlerTask, // Task function.
                            "", //name of task.
                            4096, //Stack size of task
                            NULL, // parameter of the task
                            10,  // priority of the task
                            NULL, //Task handle to keep track of created task
                            0); //core to run task on. Core 0 = WiFi, Core 1 = Arduino

  }


  int zumoNet::getNumNodes(){
    return heartbeatTimestamps.size();
  }


  void zumoNet::broadcast(const char *topic, const char *message){
    ESP_LOGD(TAG, "Broadcasting to topic %s, msg: %s", topic,message);
    char* messageString;
    cJSON* messageJSON = cJSON_CreateObject();
    cJSON_AddStringToObject(messageJSON, "sender", nodeId);
    cJSON_AddStringToObject(messageJSON, "msg", message);
    cJSON_AddStringToObject(messageJSON, "topic", topic);
    messageString = cJSON_PrintUnformatted(messageJSON);

    char addrbuf[32] = { 0 };

    struct addrinfo hints = {
    };
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_DGRAM;

    struct addrinfo *res;

    hints.ai_family = AF_INET; // For an IPv4 socket
    int err = getaddrinfo(CONFIG_MULTICAST_IPV4_ADDR,
                          NULL,
                          &hints,
                          &res);
    if (err < 0 || res == NULL) {
      ESP_LOGE(TAG, "getaddrinfo() failed for IPV4 destination address. error: %d", err);
      goto done;
    }
    ((struct sockaddr_in *)res->ai_addr)->sin_port = htons(CONFIG_UDP_PORT);
    inet_ntoa_r(((struct sockaddr_in *)res->ai_addr)->sin_addr, addrbuf, sizeof(addrbuf)-1);
    ESP_LOGI(TAG, "Sending to IPV4 multicast address %s...",  addrbuf);
    err = sendto(sock, messageString, strlen(messageString)+1, 0, res->ai_addr, res->ai_addrlen);
    freeaddrinfo(res);
    if (err < 0) {
      ESP_LOGE(TAG, "IPV4 sendto failed. errno: %d", errno);
      goto done;
    }
  done:
    cJSON_Delete(messageJSON);
    free(messageString);
  }

  void zumoNet::send(const char* target, const char* topic, const char* message){
    char* messageString;
    cJSON* messageJSON = cJSON_CreateObject();
    cJSON_AddStringToObject(messageJSON, "sender", nodeId);
    cJSON_AddStringToObject(messageJSON, "msg", message);
    cJSON_AddStringToObject(messageJSON, "topic", topic);
    messageString = cJSON_PrintUnformatted(messageJSON);

    char addrbuf[32] = { 0 };
    int err;
    struct addrinfo hints = {
    };
    hints.ai_flags = AI_PASSIVE;
    hints.ai_socktype = SOCK_DGRAM;

    struct addrinfo *res;

    hints.ai_family = AF_INET; // For an IPv4 socket
    ESP_ERROR_CHECK(getaddrinfo(target,
                          NULL,
                          &hints,
                                &res));
    if ( res == NULL) {
      ESP_LOGE(TAG, "getaddrinfo() failed for IPV4 destination address.");
      goto done;
    }
    ((struct sockaddr_in *)res->ai_addr)->sin_port = htons(CONFIG_UDP_PORT);
    inet_ntoa_r(((struct sockaddr_in *)res->ai_addr)->sin_addr, addrbuf, sizeof(addrbuf)-1);
    ESP_LOGI(TAG, "Sending to IPV4 unicast address %s...",  addrbuf);
    err = sendto(sock, messageString, strlen(messageString)+1, 0, res->ai_addr, res->ai_addrlen);
    freeaddrinfo(res);
    if (err < 0) {
      ESP_LOGE(TAG, "IPV4 sendto failed. errno: %d", errno);
      goto done;
    }
  done:
    cJSON_Delete(messageJSON);
    free(messageString);
  }
