#include "UserTask.h"
#include "Zumo.h"
#include "ZumoNet.h"
#include "cJSON.h"

/**
 * Navigator.cpp
 *
 * This is where the actual algorithm goes.
 * In this file, you can create tasks to execute your algorithm and tell the Zumo where to go.
 * To communicate with the Zumo, use the global zumo object. Refer to the documentation for the API.
 * To communicate with the network, use the functions provided in ZumoNet.cpp
 * To create a task write a function like this:
 *        void myTask(void* nothing){...}
 * The void* argument will always be NULL but is necessary to conform to the interface of FreeRTOS.
 * After creating the function, add it to the userTasks array.
 * Once the system has started up, connected to the network and initialized the Zumo, your tasks will start running.
 * For general information on writing FreeRTOS tasks, please consult the FreeRTOS documentation.
 * This file contains example code that can be replaced by your algorithm
 */

//Declare your tasks here to be able to add them to the array
void navFun(void*);//These two examples can be removed
void lockingTaskFun(void*);

//Do not forget to add your tasks to this array
void (*userTasks[])(void*) = {navFun, lockingTaskFun};

//No need to modify this
size_t numUserTasks = sizeof(userTasks)/sizeof(void (*)(void*));

//Everything below this line is the example code. Feel free to remove and replace it
#define BATCH_LOCKING_SIZE 5

std::tuple<int,int> lockedSquares[BATCH_LOCKING_SIZE];
std::tuple<int,int> squaresToLock[BATCH_LOCKING_SIZE];
enum ZumoCommand commandQueue[BATCH_LOCKING_SIZE];
enum ZumoCommand commands[] = {GO_NORTH, GO_EAST, GO_WEST, GO_SOUTH};

int commandIndex = 0;

int min(int a, int b){
  return a<b?a:b;
}

int lockCells(){
  const char* TAG ="LOCK";

  static int rqIndex;
  rqIndex++;

  unsigned replies = 0;
  unsigned n = zumoNet::getNumNodes();//TODO CHANGE THIS BACK ONCE THE MULTICAST ISSUE IS SOLVED

  cJSON *requestJSON = cJSON_CreateObject();

  cJSON * squareArray = cJSON_AddArrayToObject(requestJSON, "cells");
  for(int i = 0; i<BATCH_LOCKING_SIZE; i++){
    cJSON *cell = cJSON_CreateObject();
    cJSON_AddNumberToObject(cell, "x", std::get<0>(squaresToLock[i]));
    cJSON_AddNumberToObject(cell, "y", std::get<1>(squaresToLock[i]));
    cJSON_AddItemToArray(squareArray, cell);
  }
  cJSON_AddNumberToObject(requestJSON, "id", rqIndex);
  char *requestString = cJSON_PrintUnformatted(requestJSON);

  RingbufHandle_t msgBuffer = zumoNet::subscribe("lockrep");
  ESP_LOGI(TAG, "Requesting lock");
  zumoNet::broadcast("lockrq", requestString);
  free(requestString);
  cJSON_Delete(requestJSON);
  int ok = BATCH_LOCKING_SIZE;

  TickType_t timeout = xTaskGetTickCount() + pdMS_TO_TICKS(1000);
  size_t rcvdSize;
  ESP_LOGI(TAG, "Waiting for %u OKs", n);
  while((replies < n) && (xTaskGetTickCount() < timeout)){
    void *rep = xRingbufferReceive(msgBuffer,&rcvdSize, timeout - xTaskGetTickCount() );
    if(rep == NULL){
      ESP_LOGI(TAG, "RX timeout");
      ok = false;
      break;
    }
    struct zumoNet::message *msg = (struct zumoNet::message*) rep;

    StaticJsonBuffer<100> jsonParser;

    JsonObject &reply = jsonParser.parseObject(msg->message);

    if(!reply.success()){
      ESP_LOGW(TAG, "Parsing JSON failed");
      vRingbufferReturnItem(msgBuffer, rep);
      continue;
    }
    if(reply["id"] == rqIndex){
      replies++;
      int granted = reply["ok"];
      ok = min(ok, granted);
      ESP_LOGI(TAG, "%d OK from %s",granted, msg->sender);
    }else{
      vRingbufferReturnItem(msgBuffer, rep);
      continue;
    }
    vRingbufferReturnItem(msgBuffer, rep);
  }
  zumoNet::unsubscribe(msgBuffer);
  int res = replies == n?ok : 0;
  ESP_LOGI(TAG, "Lock: %d OK",res);
  return res;
}


void navFun(void*nothing){
  const char* TAG ="NAV";
  RingbufHandle_t buffer = zumoNet::subscribe("commands");
  size_t rcvdSize;
  QueueHandle_t zumoEvents = zumo.getEventChannel();
  int failedLockAttempts = 0;
  for(;;){
    for(int i = 0; i < BATCH_LOCKING_SIZE; i++){
      lockedSquares[i] = std::make_tuple(-1,-1);
      squaresToLock[i] = std::make_tuple(-1,-1);
      commandQueue[i] = STOP;
    }
    commandIndex = 0;
    std::tuple<int,int> previousPos = zumo.getPosition();
    for(int i = 0; i < BATCH_LOCKING_SIZE ;){
      int r;
      if(std::get<0>(previousPos) == 0 && std::get<1>(previousPos) == 0){
        r = 0;
      }else{
        r = random(4);
      }
      enum ZumoCommand cmd = commands[r];
      std::tuple<int,int> posToLock = zumo.getPositionAfterMove(cmd, previousPos);
      if(std::get<0>(posToLock) == -1 || (std::get<1>(posToLock) == 0 && std::get<0>(posToLock) == 0)){
        continue;
      }else{
        squaresToLock[i] = posToLock;
        ESP_LOGI(TAG, "new command: %c going to %d, %d", cmd, std::get<0>(posToLock), std::get<1>(posToLock));
        previousPos = posToLock;
        commandQueue[i] = cmd;
        i++;
      }
    }
    
    int ok = 0;
    
    if((ok = lockCells())){
      for(; commandIndex < ok; commandIndex++){
        failedLockAttempts = 0;
        if(zumo.sendCommand(commandQueue[commandIndex]) == ESP_OK){
          enum ZumoEvent saas;
          xQueueReceive(zumoEvents,&saas, portMAX_DELAY);
        }else{
          ESP_LOGE(TAG, "Failed to submit command to Zumo");
          break;
        }
      }
    }else{
      failedLockAttempts= min(failedLockAttempts+1, 7);
      vTaskDelay(pdMS_TO_TICKS(random(1 << failedLockAttempts) * 10));
    }
  }
}

struct requestSequence{
  char node[sizeof(zumoNet::nodeId)];
  int num;
};
//list<struct requestSequence> lastRequests;

bool grantCell(int x, int y){
  if(x == std::get<0>(zumo.getPosition()) && y == std::get<1>(zumo.getPosition())){
    return false;
  }
  for(int i = commandIndex; i < BATCH_LOCKING_SIZE; i++){
    if(x == std::get<0>(squaresToLock[i]) && y == std::get<1>(squaresToLock[i])){
      return false;
    }
  }
  return true;
}

void lockingTaskFun(void*nothing){
  static const char* TAG = "LOCKER";
  RingbufHandle_t msgBuffer = zumoNet::subscribe("lockrq");
  size_t rcvdSize;
  for(;;){
    cJSON *request;
    void *msg;
    {
      msg = xRingbufferReceive(msgBuffer, &rcvdSize, portMAX_DELAY);


        struct zumoNet::message *message = (struct zumoNet::message*) msg;

      ESP_LOGI(TAG, "RX from %s: %s", message->sender, message->message);
      int ok = 0;
      request = cJSON_Parse(message->message);
      if(request == NULL){
        goto cleanupWithoutReply;
      }

      cJSON *id = cJSON_GetObjectItem(request, "id");
      if((id == NULL) || (id->type != cJSON_Number)){
        goto cleanupWithoutReply;
      }
      if(!strncmp(message->sender, zumoNet::nodeId, strlen(zumoNet::nodeId))){
        ok = BATCH_LOCKING_SIZE;
      }else{
        cJSON *cells = cJSON_GetObjectItem(request, "cells");
        cJSON *cell;
        cJSON_ArrayForEach(cell, cells){
          cJSON *x = cJSON_GetObjectItem(cell, "x");
          cJSON *y = cJSON_GetObjectItem(cell, "y");
          if(!cJSON_IsNumber(x) || !cJSON_IsNumber(y)){
            goto cleanupWithoutReply;
          }
          ESP_LOGI(TAG, "Request for %d, %d", x->valueint, y->valueint);
          if(grantCell(x->valueint, y->valueint)){
            ok++;
          }else{
            break;
          }
        }
      }
      cJSON *reply = cJSON_CreateObject();
      cJSON_AddNumberToObject(reply, "id", id -> valueint);
      cJSON_AddNumberToObject(reply, "ok", ok);
      char *replyString = cJSON_PrintUnformatted(reply);
      zumoNet::send(message->sender, "lockrep", replyString);
      free(replyString);
      cJSON_Delete(reply);
    }
  cleanupWithoutReply:
    vRingbufferReturnItem(msgBuffer,msg);
    cJSON_Delete(request);
    
  }
}
