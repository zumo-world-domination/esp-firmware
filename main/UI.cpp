#include "UI.h"
#include "ZumoNet.h"
#include <SPIFFS.h>

static const char* TAG = "UI";

OLEDDisplayUi ui ( &display );

TaskHandle_t uiTask;

void updateUi(){
  ui.update();
}


/*
 * Functions to draw the UI
 * To add an element:
 * 1: create a new overlay function like the ones below
 * 2: add it to the array of overlay function
 * 3: adjust the number of overlays
 */

void connectionCountOverlay(OLEDDisplay *display, OLEDDisplayUiState* state) {
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_16);
  display->drawString(128, 0, String(zumoNet::getNumNodes()));
}
void nodeIdOverlay(OLEDDisplay *display, OLEDDisplayUiState* state){
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  display->drawString(64, 0, zumoNet::nodeId);
}

void timestampOverlay(OLEDDisplay *display, OLEDDisplayUiState* state){
  display->setTextAlignment(TEXT_ALIGN_CENTER);
  display->setFont(ArialMT_Plain_10);
  display->drawString(64, 54, String(millis()));
}

void loggerOverlay(OLEDDisplay *display, OLEDDisplayUiState* state){
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_10);
  display->drawString(128,54,(xEventGroupGetBits(systemFlags) & MULTICAST_SOCKET_OPEN) ? "CONN" : "");
}

void nOverlay(OLEDDisplay *display, OLEDDisplayUiState* state){
  display->setTextAlignment(TEXT_ALIGN_LEFT);
  display->setFont(ArialMT_Plain_10);
  display->drawString(0,0,(xEventGroupGetBits(systemFlags) & N_KNOWN) ? "N" : "");
}


void heapOverlay(OLEDDisplay *display, OLEDDisplayUiState* state){
  display->setTextAlignment(TEXT_ALIGN_RIGHT);
  display->setFont(ArialMT_Plain_10);
  display->drawString(128,22,"RAM");
  display->drawString(128,32,String(ESP.getFreeHeap()));
}

bool techLoaded = false;
char techBuffer[128];
char tekkuBuffer[384];

void drawFrame1(OLEDDisplay *display, OLEDDisplayUiState* state, int16_t x, int16_t y) {
  if(!techLoaded){
    ESP_LOGI(TAG, "Loading images");
    File techFile = SPIFFS.open("/titech_32x32.bin");
    File tekkuFile = SPIFFS.open("/tekku_42x64.bin");
    if(!techFile){
      ESP_LOGE(TAG,"failed to open logo");
      return;
    }
    if(!tekkuFile){
      ESP_LOGE(TAG,"failed to open tech-chan");
      return;
    }

    int techSize = techFile.size();
    int tekkuSize = tekkuFile.size();
    techFile.seek(0,SeekSet);
    techFile.readBytes(techBuffer, techSize);
    tekkuFile.seek(0,SeekSet);
    tekkuFile.readBytes(tekkuBuffer, tekkuSize);
    techLoaded = true;
  }
   display->drawXbm(48,16,32,32,(uint8_t*)techBuffer);
   display->drawXbm(0,0,42,64,(uint8_t*)tekkuBuffer);
}

//Add the overlay function here
OverlayCallback overlays[] = { connectionCountOverlay, nodeIdOverlay, timestampOverlay , loggerOverlay,heapOverlay, nOverlay};
int overlaysCount = sizeof(overlays) / sizeof(OverlayCallback);


FrameCallback frames[] = { drawFrame1};
int frameCount = sizeof(frames) / sizeof(FrameCallback);

void uiTaskFunction(void *args){
  ui.setTargetFPS(UI_FPS);
  ui.setOverlays(overlays, overlaysCount);
  ui.setFrames(frames, frameCount);
  ui.disableAllIndicators();
  // Initialising the UI will init the display too.
  ui.setTimePerFrame(1000/UI_FPS);
  ui.disableAutoTransition();
  ui.init();
  display.clear();
  display.setContrast(255);
  display.flipScreenVertically();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.setFont(ArialMT_Plain_10);
  display.drawString(0,0,"Loading...");
  display.display();

  xEventGroupSetBits(systemFlags, UI_READY);
  for(;;){
    ui.update();
    vTaskDelay(pdMS_TO_TICKS(1000/UI_FPS));
  }
}

void initUI(uint8_t core){

  xTaskCreatePinnedToCore(
                          uiTaskFunction, // Task function.
                          "ui", //name of task.
                          5000, //Stack size of task
                          NULL, // parameter of the task
                          0,  // priority of the task
                          &uiTask, //Task handle to keep track of created task
                          core); //core to run task on. Core 0 = WiFi, Core 1 = Arduino

}
