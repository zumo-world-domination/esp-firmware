//************************************************************
// this is a simple example that uses the easyMesh library
//
// 1. blinks led once for every node on the mesh
// 2. blink cycle repeats every BLINK_PERIOD
// 3. sends a silly message to every node on the mesh at a random time between 1 and 5 seconds
// 4. prints anything it receives to Serial.print
//
//
//************************************************************


#include <SPIFFS.h>
#include "Hardware.h"
#include "ZumoNet.h"
#include "UI.h"
#include <FreeRTOS.h>
#include <esp_freertos_hooks.h>
#include "haltAndCatchFire.h"
#include "Zumo.h"
#include <esp_log.h>
#include "Grid.h"
#include "systemFlags.h"
#include "UserTask.h"
#include "Navigator.h"

/**
 *
 * Main function
 *
 * Not much going on here, just initialization of the system and user tasks
 **/

static const char TAG[] = "MAIN";

unsigned core0Counter = 0;
unsigned core1Counter = 0;

static bool core0CounterCb(void){
  core0Counter++;
  return false;
}
static bool core1CounterCb(void){
  core1Counter++;
  return false;
}

static void printCpuLoad(void *nothing){
  for(;;){
    ESP_LOGE("IDLE", "Core 0: %d Core 1: %d", core0Counter, core1Counter);
    core0Counter = core1Counter = 0;
    vTaskDelay(pdMS_TO_TICKS(1000));
  }
}

extern "C"{
void app_main() {
  initArduino();
  if(!SPIFFS.begin()){
    haltAndCatchFire("SPIFFS ERROR");
    return;
  }else{
    xEventGroupSetBits(systemFlags, SPIFFS_MOUNTED);
  }

  //esp_register_freertos_idle_hook_for_cpu(core0CounterCb, 0);
  //  esp_register_freertos_idle_hook_for_cpu(core1CounterCb, 1);

  /*xTaskCreatePinnedToCore(
                          printCpuLoad, // Task function.
                          "", //name of task.
                          2048, //Stack size of task
                          NULL, // parameter of the task
                          15,  // priority of the task
                          NULL, //Task handle to keep track of created task
                          1); //core to run task on. Core 0 = WiFi, Core 1 = Arduino
  */

  zumoNet::init(0);

  initUI(1);

  uart_set_pin(UART_NUM_1, GPIO_NUM_26, GPIO_NUM_25, UART_PIN_NO_CHANGE,UART_PIN_NO_CHANGE);

  const uart_config_t uart_config = {
                                     .baud_rate = 115200,
                                     .data_bits = UART_DATA_8_BITS,
                                     .parity = UART_PARITY_DISABLE,
                                     .stop_bits = UART_STOP_BITS_1,
                                     .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
  };

  uart_param_config(UART_NUM_0, &uart_config);
  esp_err_t uart_err = uart_driver_install(UART_NUM_0, UART_FIFO_LEN*2, 0 , 0, NULL, 0);


  randomSeed(analogRead(A0));
  zumo.init();
  xEventGroupWaitBits(systemFlags,   //The event group being tested.
                      N_KNOWN|MULTICAST_SOCKET_OPEN|ZUMO_READY, //The bits within the event group to wait for.
                      pdFALSE,        //clear all bits after leaving
                      pdTRUE,       //wait for all bits to be set
                      portMAX_DELAY);//wait forever
  vTaskDelay(pdMS_TO_TICKS(2*CONFIG_HEARTBEAT));//wait for some heartbeats to come in
  ESP_LOGI(TAG, "Starting %d user tasks", numUserTasks);

  for(int i = 0; i < numUserTasks; i++){
    xTaskCreatePinnedToCore(
                            userTasks[i], // Task function.
                            "", //name of task.
                            USER_STACK_SIZE, //Stack size of task
                            NULL, // parameter of the task
                            USER_PRIORITY,  // priority of the task
                            NULL, //Task handle to keep track of created task
                            1); //core to run task on. Core 0 = WiFi, Core 1 = Arduino

  }
}
}

