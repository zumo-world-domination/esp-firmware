#include "Zumo.h"
#include "haltAndCatchFire.h"
#include <esp_log.h>
#include "ZumoNet.h"
#include "systemFlags.h"
static const char* TAG = "Zumo";

Zumo zumo = Zumo(UART_NUM_1, std::make_tuple(START_X, START_Y), std::make_tuple(GRID_SIZE_X,GRID_SIZE_Y));

void Zumo::zumoReceiveFunction(void *arg_zumo){
  Zumo *zumo = (Zumo *) arg_zumo;

  ESP_LOGI(TAG,"init UART %hhu", zumo->input);
   const uart_config_t uart_config = {
                                     .baud_rate = 115200,
                                     .data_bits = UART_DATA_8_BITS,
                                     .parity = UART_PARITY_DISABLE,
                                     .stop_bits = UART_STOP_BITS_1,
                                    .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
  };
   uart_param_config(zumo->input, &uart_config);
  esp_err_t uart_err = uart_driver_install(zumo->input, UART_FIFO_LEN*2, 0 , 0, NULL, 0);
  if(uart_err == ESP_OK){
    ESP_LOGI(TAG, "UART init success");
  }else{
    ESP_LOGE(TAG,"UART DRIVER ERROR %d",uart_err);
    haltAndCatchFire("UART DRIVER ERROR");
    }


  uart_write_bytes(zumo->input,"ZUMOINIT",8);


  char response[3];
  size_t read = uart_read_bytes(zumo->input, (uint8_t*)response, 3, pdMS_TO_TICKS(30000));
  if(read != 3){
    ESP_LOGE(TAG,"ZUMO INIT TIMEOUT");
    haltAndCatchFire("ZUMO INIT TIMEOUT");
  }
  if((strncmp("RDY", response,3) != 0)){
    ESP_LOGE(TAG,"ZUMO INIT ERROR. Got %.3s instead of RDY",response);
    haltAndCatchFire("ZUMO INIT ERROR");
  }
  ESP_LOGI(TAG, "Get ready to rumble!!!");
  xEventGroupSetBits(systemFlags, ZUMO_READY);
  for(;;){
    uart_read_bytes(zumo->input, (uint8_t*)response, 2, portMAX_DELAY);
    if(response[0] == 'E'){
      switch((enum ZumoEvent)response[1]){
      case COMMAND_COMPLETE:{
        switch(zumo->currentCommand){
        case GO_NORTH:
          std::get<1>(zumo->position) += 1;
          break;
        case GO_SOUTH:
          std::get<1>(zumo->position) -= 1;
          break;
        case GO_EAST:
          std::get<0>(zumo->position) += 1;
          break;
        case GO_WEST:
          std::get<0>(zumo->position) -= 1;
          break;
        default:
          ESP_LOGE(TAG,"THE ZUMO DID SOMETHING BUT WE DONT KNOW WHAT IT WAS");
          break;
        }
        ESP_LOGD(TAG,"done");
        zumo->currentCommand = STOP;
        break;
      }
      case COMMAND_ABORTED:
        break;
      case HALT_AND_CATCH_FIRE:
        ESP_LOGE(TAG,"ZUMO INTERNAL ERROR");
        haltAndCatchFire("ZUMO INTERNAL ERROR");
        break;
      }
      for(auto const &it: zumo->eventListeners){
        if(xQueueSend(it, response+1, 0) != pdTRUE){
          ESP_LOGE(TAG, "Zumo event queue overflow");
        }
      }
    }else{
      ESP_LOGE(TAG,"INVALID MSG FROM ZUMO");
      haltAndCatchFire("ZUMO COMM ERROR");
    }
  }
}


void  Zumo::init(){
  xTaskCreatePinnedToCore(
                          this->zumoReceiveFunction, // Task function.
                          "zumo", //name of task.
                          10000, //Stack size of task
                          (void*)this, // parameter of the task
                          1,  // priority of the task
                          &task, //Task handle to keep track of created task
                          1); //core to run task on. Core 0 = WiFi, Core 1 = Arduino

}

Zumo::Zumo(uart_port_t uart, std::tuple<int,int> startingPosition, std::tuple<int,int> gridSize): input(uart), currentCommand(STOP), position(startingPosition), gridSize(gridSize) {
  input = uart;
}
esp_err_t Zumo::sendCommand(enum ZumoCommand command){
  if(currentCommand != STOP){
    ESP_LOGW(TAG, "Zumo busy, command rejected");
    return EBUSY;
  }
  switch(command){
  case STOP:
    break;
  case GO_NORTH:
    if(std::get<1>(position) == std::get<1>(gridSize)-1){
      ESP_LOGE(TAG, "Tried to leave grid towards the north");
      return ESP_FAIL;
    }
    break;
  case GO_EAST:
    if(std::get<0>(position) == std::get<0>(gridSize)-1){
      ESP_LOGE(TAG, "Tried to leave grid towards the east");
      return ESP_FAIL;
    }
    break;
  case GO_SOUTH:
    if(std::get<1>(position) == 0){
      ESP_LOGE(TAG, "Tried to leave grid towards the south");
      return ESP_FAIL;
    }
    break;
  case GO_WEST:
    if(std::get<0>(position) == 0){
      ESP_LOGE(TAG, "Tried to leave grid towards the west");
      return ESP_FAIL;
    }
    break;
  default:
    ESP_LOGE(TAG, "UNKNOWN COMMAND %d", command);
    return ENOSYS;
    break;
  }
  currentCommand = command;
  uart_write_bytes(input, "ZUMO", 4);
  uart_write_bytes(input, (char*)&command, 1);
  return ESP_OK;
}

QueueHandle_t Zumo::getEventChannel(){
  QueueHandle_t channel = xQueueCreate(2, sizeof(enum ZumoEvent));
  eventListeners.push_back(channel);
  return channel;
}

std::tuple<int,int> Zumo::getPosition(){
  return position;
}
std::tuple<int,int> Zumo::getGridSize(){
  return gridSize;
}

std::tuple<int,int> Zumo::getPositionAfterMove(enum ZumoCommand cmd, std::tuple<int,int> startPosition){
  std::tuple<int,int> newPos = startPosition;
  switch(cmd){
  case STOP:
    return newPos;
    break;
  case GO_NORTH:
    if(std::get<1>(startPosition) >= std::get<1>(gridSize)-1){
      return std::make_tuple(-1,-1);
    }
    break;
  case GO_EAST:
    if(std::get<0>(startPosition) >= std::get<0>(gridSize)-1){
      return std::make_tuple(-1,-1);
    }
    break;
  case GO_SOUTH:
    if(std::get<1>(startPosition) <= 0){
      return std::make_tuple(-1,-1);

    }
    break;
  case GO_WEST:
    if(std::get<0>(startPosition) <= 0){
      return std::make_tuple(-1,-1);
      }
    break;
  default:
    ESP_LOGE(TAG, "UNKNOWN COMMAND %d", cmd);
    return std::make_tuple(-1,-1);
    break;
  }
  switch(cmd){
  case GO_NORTH:
    std::get<1>(newPos) += 1;
    break;
  case GO_SOUTH:
    std::get<1>(newPos) -= 1;
    break;
  case GO_EAST:
    std::get<0>(newPos) += 1;
    break;
  case GO_WEST:
    std::get<0>(newPos) -= 1;
    break;
  default:
    break;
  }
  return newPos;
}
