#ifndef BLACKBOX_H
#define BLACKBOX_H
#include <Arduino.h>
#include <freertos/queue.h>
#include <driver/uart.h>
#include <base64.h>
#include <ArduinoJson.h>
#include "ZumoNet.h"
#include "ZumoProtocol.h"
#include "MessageChannel.h"
#include "Grid.h"
#include <tuple>
#include <stdlib.h>
#include <errno.h>
#include <list>
#define MAX_LOG_FRAME_SIZE 300

enum ZUMO_STATE{UNINITIALIZED,RECEIVING_START_MARKER,WAIT_FOR_HEADER,RECEIVING_HEADER,WAIT_FOR_FRAME, FRAME, EVENT};


class Zumo{
 private:
  uart_port_t input;
  QueueHandle_t uart_event_queue;
  static void zumoReceiveFunction(void *queueHandle);
  enum ZumoCommand currentCommand;
  std::list<QueueHandle_t> eventListeners;
  std::tuple<int,int> position;
  std::tuple<int,int> gridSize;
 public:
  Zumo(uart_port_t uart, std::tuple<int,int> startingPosition, std::tuple<int,int> gridSize);
  void init(void);
  TaskHandle_t task;
  esp_err_t sendCommand(enum ZumoCommand command);
  std::tuple<int,int> getPosition();
  std::tuple<int,int> getGridSize();
  QueueHandle_t getEventChannel();
  std::tuple<int,int> getPositionAfterMove(enum ZumoCommand cmd, std::tuple<int,int> start=std::make_tuple(-1,-1));
};
extern Zumo zumo;

#endif
