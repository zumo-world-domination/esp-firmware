
#ifndef ZUMONET_H
#define ZUMONET_H
#include <stdlib.h>
#include <string>
#include <freertos/FreeRTOS.h>
#include <freertos/ringbuf.h>

#define SUBSCRIBER_BUFFER_SIZE 1024
extern size_t logServerId ;

namespace zumoNet{

struct messageSubscription{
  std::string topic;
  RingbufHandle_t buffer;
};

  extern char nodeId[16];
struct message{
  char sender[sizeof(nodeId)];
  char message[];
};
  RingbufHandle_t subscribe(std::string topic);
  void unsubscribe(RingbufHandle_t subscription);
  bool waitForLogger(TickType_t = portMAX_DELAY);
  void broadcast(const char *topic, const char *msg);
  void send(const char* target, const char* topic, const char*msg);
  void init(int cpuCore);
  int getNumNodes();
}

#endif
