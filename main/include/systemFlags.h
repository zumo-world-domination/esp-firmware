#ifndef SYSTEM_FLAGS_H
#define SYSTEM_FLAGS_H
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>

#define WIFI_CONNECTED (1 << 0)
#define MULTICAST_SOCKET_OPEN (1 << 1)
#define UI_READY (1 << 3)
#define SPIFFS_MOUNTED (1 << 4)
#define ZUMO_READY (1 << 5)
#define N_KNOWN (1 << 6)
extern EventGroupHandle_t systemFlags;
#endif
