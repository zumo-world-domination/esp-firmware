#include <FreeRTOS.h>
#include <freertos/queue.h>
#include <esp_err.h>

template <typename T>
class MessageChannel{
 private:
  QueueHandle_t handle;
 public:
  MessageChannel(size_t size);
  esp_err_t receive(T* buffer, int timeout_ms = 0);
  esp_err_t send(T *item, int timeout_ms = 0);
};
