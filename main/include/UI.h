#ifndef UI_H
#define UI_H
#include <OLEDDisplayUi.h>
#include "Hardware.h"
#include <painlessMesh.h>
#include "systemFlags.h"
#define UI_FPS 2
void initUI(uint8_t core);
extern TaskHandle_t uiTask;
#endif
