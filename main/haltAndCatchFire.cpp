#include "haltAndCatchFire.h"
#include "UI.h"

void haltAndCatchFire(char* error){
  Serial.println(error);
  display.clear();
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0,0,error);
  display.display();
  for(;;);
}
